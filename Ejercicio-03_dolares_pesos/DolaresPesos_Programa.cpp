#include <iostream>
#include <limits>
#include <string>
using namespace std;

enum Opciones{
	Opcion_Invalida,
	Opcion_Positiva,
	Opcion_Negativa,
};

Opciones resolverOpciones (string entrada){
	if((entrada == "y") || (entrada == "Y")) return Opcion_Positiva;
	if((entrada == "n") || (entrada == "N")) return Opcion_Negativa;
	return Opcion_Invalida;
}

int main(){
	long double dolaresUsuario, pesosUsuario;
	bool controlPrograma, controlRetorno;
	string respuestaUsuario;
	
	controlPrograma = true;
	
	while(controlPrograma){
		cout << "Bienvenido. Por favor ingresar la cantidad en d\xA2lares que desea conocer su equivalencia en pesos: "; cin >> dolaresUsuario;
		if(cin.fail()){
			cin.clear();
			cin.ignore(numeric_limits<int>::max(),'\n');
			cout << endl << "Lo sentimos, usted a ingresado un valor no num\x82rico. Por favor intentelo de nuevo." << endl;
		}else{
			controlPrograma = false;
			controlRetorno = true;
			pesosUsuario = (dolaresUsuario * 21.96);
			cout << "La equivalencia en pesos es de: " << pesosUsuario << endl;
			while(controlRetorno){
				cout << "\xA8Le gustar\xA1a volver a ingresar otro valor?(y/n)"; cin >> respuestaUsuario;
				switch(resolverOpciones(respuestaUsuario)){
					case Opcion_Positiva:
						controlPrograma = true;
						controlRetorno = false;
						break;
					case Opcion_Negativa:
						controlRetorno = false;
						break;
					default:
						cout << "Lo sentimos, usted a ingresado una instrucci\xA2n erronea. Por favor ingresar alguna de las dos mostradas entre los parentesis.";
						break;
				}
			}
		}
	}
}
