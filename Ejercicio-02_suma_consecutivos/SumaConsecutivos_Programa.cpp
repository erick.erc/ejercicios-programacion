#include <iostream>
#include <limits>
#include <string>
using namespace std;

enum Opciones{
	Opcion_Invalida,
	Opcion_Positiva,
	Opcion_Negativa,
};

Opciones resolverOpciones (string entrada){
	if((entrada == "y") || (entrada == "Y")) return Opcion_Positiva;
	if((entrada == "n") || (entrada == "N")) return Opcion_Negativa;
	return Opcion_Invalida;
}

int main(){
	int valorMeta, valorResultante, i;
	bool controlPrograma, controlRetorno;
	string respuestaUsuario;
	
	valorResultante=0;
	controlPrograma=true;
	
	while(controlPrograma){
		cout << "Bienvenido. Por favor ingresar un n\xA3mero entero que este en el tango del 1 al 50: "; cin >> valorMeta;
		if(cin.fail()){
			cin.clear();
			cin.ignore(numeric_limits<int>::max(),'\n');
			cout << "Lo sentimos, usted a ingresado un valor no num\x82rico. Por favor intentelo de nuevo." << endl;
		}else{
			if((valorMeta < 1) || (valorMeta > 50)){
				cin.clear();
				cin.ignore(numeric_limits<int>::max(),'\n');
				cout << "Lo sentimos, usted a ingresado un valor num\x82rico fuera del rango. Por favor intentelo de nuevo." << endl;
			}else{
				controlPrograma=false;
				controlRetorno=true;
				for(i=0; i<=valorMeta; i++){
					valorResultante += i;
				}
				cout << "La suma de consecutivos resultante es: " << valorResultante << endl;
				while(controlRetorno){
					cout << "\xA8Le gustar\xA1a volver a ingresar otro valor?(y/n)"; cin >> respuestaUsuario;
					switch(resolverOpciones(respuestaUsuario)){
						case Opcion_Positiva:
							valorResultante = 0;
							controlPrograma = true;
							controlRetorno = false;
							break;
						case Opcion_Negativa:
							controlRetorno = false;
							break;
						default:
							cout << "Lo sentimos, usted a ingresado una instrucci\xA2n erronea. Por favor ingresar alguna de las dos mostradas entre los parentesis.";
							break;
					}
				}
			}
		}
	}
}
