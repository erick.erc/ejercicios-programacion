Inicio
	Variable valorMeta es Numérico Natural
	Variable valorResultante es Numérico Natural
	Variable controlPrograma es Booleana
	Variable controlRetorno es Booleana
	Variable respuestaUsuario es una Cadena de caracteres
	
	
	valorResultante = 0;
	controlPrograma = True
	
	while(controlPrograma)
		Leer "Bienvenido. Por favor ingresar un número entero que este en el rango del 1 al 50: \n", valorMeta
		if (Leer.fail())
			Escribir "Lo sentimos, usted a ingresado un valor no numerico. Por favor intentelo de nuevo. \n"
		else
			if ((valorMeta < 1) || (valorMeta > 50))
				Escribir "Lo sentimos, usted a ingresado un valor numérico fuera del rango. Por favor intentelo de nuevo. \n"
			else
				controlPrograma = False
				controlRetorno = True
				for(i=0; i<=valorMeta; i++)
					valorResultante += i
				end-for
				Escribir "La suma de consecutivos resultante es: ", valorResultante, \n
				while(controlRetorno)
					Leer "¿Le gustaría volver a ingresar otro valor?(y/n)", respuestaUsuario
					switch(respuestaUsuario)
						case'y','Y':
							controlPrograma = True
							controlRetorno = False
							break	
						case'n','N':
							controlRetorno = False
							break
						default
							Escribir "Lo sentimos, usted a ingresado una instrucción erronea. Por favor ingresar alguna de las dos mostradas entre los parentesis."
					switch-end
				end-while
			end-if
		end-if
	end-while
Fin